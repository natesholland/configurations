configurations
==============

These are just some configurations file that I like, anyone is welcome to use them and if you have any suggestions please email me: natesholland@gmail.com

So far all I have is sublime text and vim since those are the only things I use that are configurable.

### Userful links:

[Git tricks](http://git-scm.com/book/en/Git-Basics-Tips-and-Tricks)
